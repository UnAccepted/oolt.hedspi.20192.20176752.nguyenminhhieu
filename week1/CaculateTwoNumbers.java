//Example 5: ShowTwoNumbers.java
import javax.swing.JOptionPane;
//
public class CaculateTwoNumbers {
  public static void main(String[] args) {
    String strNum1, strNum2;
    strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
    strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
    Double Num1 = Double.parseDouble(strNum1),
    Num2 = Double.parseDouble(strNum2);
    JOptionPane.showMessageDialog(null,"Sum: " + (Num1 + Num2) + "\nDifference: " + (Num1 - Num2) + "\nProduct: " + (Num1 * Num2) + "\nQuotient: " + ((int)(Num1 / Num2)), "Caculate", JOptionPane.INFORMATION_MESSAGE);
    System.exit(0);
  }
}
