import java.util.*;
import java.text.*;

public class MyDate {
	private int day;
	private int month;
	private int year;
	private Date date;
	
	String[] monthInString = new String[]{"x", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"},
			dayInString = new String[] {"x", "1st", "2nd", "3th", "4th", "5th", "6th", "7th", "8th", "9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th", "21th", "22th", "23th", "24th", "25th", "26th", "27th", "28th", "29th", "30th", "31th"},
			dayCall = new String[] {"x", " first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second", "twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth", "thirtieth", "thirty-first"};
	public MyDate() {
		Calendar calendar = Calendar.getInstance();
		this.day = calendar.get(Calendar.DATE);
		this.month = calendar.get(Calendar.MONTH)+1;
		this.year = calendar.get(Calendar.YEAR);
	}
	
	public MyDate(int day, int month, int year) {
		if (new CheckYMD().checkYMD(year, month, day)) {
			this.day = day;
			this.month = month;
			this.year = year;
		} else System.out.println("Invalid Date!");
	}	
	//Work to do: have to check valid date
	public MyDate(String date) {
		dateInStringToDate(date);
	}
	
	public void accept() {
		String date;
		Scanner scan = new Scanner(System.in);
		System.out.print("Input date (EX: February 18th 2019): ");
		date = scan.nextLine();
		dateInStringToDate(date);
		scan.close();
	}
	//
	public void setX(String day) {
		
	}
	//
	private void dateInStringToDate(String date) {
		int i, d, m, y;
		//February 18th 2019
		date = date.trim();
		String[] dateInString = date.split("\\s");//splits the string based on whitespace 
		
		//check valid of month
		i = 1;
		String monthInLowerCase = dateInString[0].toLowerCase();
		while((i <= 12) && (monthInLowerCase.equals(monthInString[i]) == false)) i=i+1;

		if(i > 12) {
			System.out.println("Invalid date!");
			return;
		} else m = i;
		
		//check valid of day
		i = 1;
		String dayInLowerCase = dateInString[1].toLowerCase();
		while((i <= 31) && (dayInLowerCase.equals(dayInString[i]) == false)) i=i+1;
		
		if(i > 31) {
			System.out.println("Invalid date!");
			return;
		} else d = i;
		
		y = Integer.parseInt(dateInString[2]);
		
		if (new CheckYMD().checkYMD(y, m, d)) {
			this.day = d;
			this.month = m;
			this.year = y;
		} else System.out.println("Invalid Date!");
	}
	//
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Date getDate( ) {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	//
	public Date convertDate() {
		SimpleDateFormat temp = new SimpleDateFormat("dd/MM/yyyy");
		Date res = new Date();
		String currentDate = new String();
		currentDate = String.valueOf(getDay()) + "/" + String.valueOf(getMonth()) + "/" + String.valueOf(getYear());
		try {
			 res = temp.parse(currentDate);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public void print() {
		if (new CheckYMD().checkYMD(getYear(), getMonth(), getDay())) System.out.println("Current date is: " + this.day + "-" + this.month + "-" + this.year);
		else System.out.println("Invalid Date to print!");
	}

}
