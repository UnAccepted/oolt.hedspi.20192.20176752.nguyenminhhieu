package hust.soict.ictglobal.aims.media;

import java.util.*;

public class Book extends Media{
	
	private List<String> authors = new ArrayList<String>();
	public Book() {
		// TODO Auto-generated constructor stub
	}
	//
	public Book(String title){
		super();
		this.title = title;
	}
	public Book(String title, String category){
		super();
		this.title = title;
		this.category = category;
	}
	public Book(String title, String category, List<String> authors){
		super();
		this.title = title;
		this.category = category;
		this.authors = authors;
	}
	//
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName) {
		if (this.authors.contains(authorName)) System.out.println("This author is already exist");
		else authors.add(authorName);
	}
	public void removeAuthor(String authorName) {
		if (!authors.remove(authorName)) System.out.println("This list doesn't have any author like that");
	}

}
