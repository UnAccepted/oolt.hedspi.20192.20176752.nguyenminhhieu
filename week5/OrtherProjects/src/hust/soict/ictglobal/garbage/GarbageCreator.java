package hust.soict.ictglobal.garbage;

import java.io.*;
import java.util.Scanner; 

public class GarbageCreator {
	public static void main(String[] args) throws Exception 
		{ 
			// pass the path to the file as a parameter 
			File file = new File("garbage.txt"); 
			Scanner sc = new Scanner(file); 
			long start = System.currentTimeMillis();
			String data = "";
			while (sc.hasNextLine()) {
				data += sc.nextLine();
		        //System.out.println(data);
			}
			System.out.println(System.currentTimeMillis() - start);
	  }
}
