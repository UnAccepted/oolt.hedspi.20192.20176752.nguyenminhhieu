package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

public class DigitalVideoDisc {
	public DigitalVideoDisc(String title, String category, String director) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
	}
	public DigitalVideoDisc(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}
	public DigitalVideoDisc(String title) {
		super();
		this.title = title;
	}
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	//
	private String title, category, director;
	private int length;
	private float cost;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	//
	public boolean search(String title) {
		title = title.trim();
		String tmp = this.title;
		tmp = tmp.trim();
		String[] tmp1 = title.split("\\s"),
				tmp2 = tmp.split("\\s");
		for (int i = 0; i < tmp1.length; i++)
			for (int j = 0; j < tmp2.length; j++)
				if (tmp1[i].equalsIgnoreCase(tmp2[j])) return true;
		return false;
	}
}
