package hust.soict.ictglobal.aims.order.Order;

import java.util.ArrayList;
import java.util.Date;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.media.Book;

public class Order {
	
	final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 3;
	//
	private Date dateOrder = new Date();
	public static int nbOrders = 0;
	public ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private ArrayList<DigitalVideoDisc> discOrdered = new ArrayList<DigitalVideoDisc>();
	private ArrayList<Book> bookOrdered = new ArrayList<Book>();
	//
	public Order(int check) {
		if (check == 1) {
			nbOrders = nbOrders + 1;
			if(nbOrders < MAX_LIMITTED_ORDERS) System.out.println("nbOrder is below to the MAX_LIMITTED_ORDERS");
			else {
				if(nbOrders==MAX_LIMITTED_ORDERS) System.out.println("nbOrder have reached the MAX_LIMITTED_ORDERS");
				else System.out.println("nbOrder have exceeded the MAX_LIMITTED_ORDERS");	
			}
		}
	}
	//
	private int id = nbOrders;
	//
	public Date getDateOrder() {
		return dateOrder;
	}
	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}
	//
	//public DigitalVideoDisc getALuckyItem() {
	//	int rand = (int)(Math.random() * (itemsOrdered.length - 1));
	//	itemsOrdered[rand].setCost(0);
	//	System.out.println("You are lucky because your order's " + itemsOrdered[rand].getTitle());
	//	return itemsOrdered[rand];
	//}
	//
	 public void addMedia(Media media) {
		    if (itemsOrdered.size() <= MAX_NUMBERS_ORDERED) {
		      itemsOrdered.add(media);
		      System.out.println("The media has been added!");
		    } else {
		      System.out.println("The order is full!");
		    }
		  }

		  public void removeMedia(Media media) {
		    if (itemsOrdered.remove(media)) {
		      System.out.println("The disc has been removed!");
		    } else {
		      System.out.println("Media is not in order!");
		    }
		  }
	//
	public float totalCost() {
		float res = 0;
		for (int i = 0; i < itemsOrdered.size(); i++) res += itemsOrdered.get(i).getCost();
		return res;
	}
	//
	public void printOrder() {
		System.out.println("*********************Order*************************");
		System.out.println("DATE: " + this.dateOrder.toString());
		System.out.println("Ordered Items: ");
		for (int i = 0; i < itemsOrdered.size(); i++) {
			System.out.print((i+1));
			itemsOrdered.get(i).printMedia();
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("***************************************************");
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
