//First degree equation two variables
import javax.swing.JOptionPane;
//
public class FDE2V {
  public static void main(String[] args) {
    JOptionPane.showMessageDialog(null,"a * x + b * y = c", "Caculate", JOptionPane.INFORMATION_MESSAGE);
    String stra = JOptionPane.showInputDialog(null, "Please input the a number: ", "Input the a number", JOptionPane.INFORMATION_MESSAGE),
    strb = JOptionPane.showInputDialog(null, "Please input the b number: ", "Input the b number", JOptionPane.INFORMATION_MESSAGE),
    strc = JOptionPane.showInputDialog(null, "Please input the c number: ", "Input the c number", JOptionPane.INFORMATION_MESSAGE);
    Double a = Double.parseDouble(stra),
    b = Double.parseDouble(strb),
    c = Double.parseDouble(strc);
    if ((a == 0) && (b == 0)) {
      if (c == 0) JOptionPane.showMessageDialog(null,"Infiniti Solution", "Result", JOptionPane.INFORMATION_MESSAGE);
      else JOptionPane.showMessageDialog(null,"No Solution", "Result", JOptionPane.INFORMATION_MESSAGE);
    } else {
      if (a == 0) JOptionPane.showMessageDialog(null,"Solution y = " + (c / b), "Result", JOptionPane.INFORMATION_MESSAGE);
      else if (b == 0) JOptionPane.showMessageDialog(null,"Solution x = " + (c / a), "Result", JOptionPane.INFORMATION_MESSAGE);
      else JOptionPane.showMessageDialog(null,"Solution y = " + (c / b) + " - " + (c / a) + " * x", "Result", JOptionPane.INFORMATION_MESSAGE);
    }
    System.exit(0);
  }
}
