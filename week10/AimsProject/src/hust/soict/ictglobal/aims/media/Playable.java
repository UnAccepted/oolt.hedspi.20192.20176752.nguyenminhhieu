package hust.soict.ictglobal.aims.media;

import	hust.soict.ictglobal.aims.*;

public interface Playable{
	public void play() throws PlayerException;
}
