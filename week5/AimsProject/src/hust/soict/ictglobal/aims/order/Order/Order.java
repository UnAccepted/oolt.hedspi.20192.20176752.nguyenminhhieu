package hust.soict.ictglobal.aims.order.Order;
import java.lang.Math;
import java.util.Date;
import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.*;;

public class Order {
	final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 3;
	//2 instance variables
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered;
	private Date dateOrder = new Date();
	private static int nbOrders = 0;
	//
	public Order() {
		nbOrders = nbOrders + 1;
		 if(nbOrders < MAX_LIMITTED_ORDERS)
			 System.out.println("nbOrder is below to the MAX_LIMITTED_ORDERS");
		 else if(nbOrders==MAX_LIMITTED_ORDERS)
			 System.out.println("nbOrder have reached the MAX_LIMITTED_ORDERS");
		 else System.out.println("nbOrder have exceeded the MAX_LIMITTED_ORDERS");
		
	}
	//
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public Date getDateOrder() {
		return dateOrder;
	}
	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}
	//
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		int n = getQtyOrdered();
		if (n < MAX_NUMBERS_ORDERED) {
			itemsOrdered[n] = disc;
			setQtyOrdered(n+1);
			System.out.println("The disc " + disc.getTitle() + " has been added");
		} else {
			System.out.println("The disc is already full");
			System.out.println("Can't add "+ disc.getTitle());
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		int m = dvdList.length;
		for (int i = 0; i < m; i++) addDigitalVideoDisc(dvdList[i]);
	}
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		addDigitalVideoDisc(dvd1);
		addDigitalVideoDisc(dvd2);
	}
	//
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int i = 0,
			n = getQtyOrdered();
		while (i < n) {
			if (itemsOrdered[i] == disc) {
				for (int j = i; j < n-1; j++) itemsOrdered[j] = itemsOrdered[j+1];
				setQtyOrdered(n-1);
				System.out.println("The disc " + disc.getTitle() + " has been removed");
				return;
			}
			i++;
		}
		System.out.println("There are no " + disc.getTitle() + " in this order");
	}
	//
	public DigitalVideoDisc getALuckyItem() {
		int rand = (int)(Math.random() * (itemsOrdered.length - 1));
		itemsOrdered[rand].setCost(0);
		System.out.println("You are lucky because your order's " + itemsOrdered[rand].getTitle());
		return itemsOrdered[rand];
	}
	//
	public float totalCost() {
		float res = 0;
		for (int i = 0; i < getQtyOrdered(); i++) res += itemsOrdered[i].getCost();
		return res;
	}
	//
	public void printOrder() {
		System.out.println("*********************Order*************************");
		System.out.println("DATE: " + this.dateOrder.toString());
		System.out.println("Ordered Items: ");
		for (int i = 0; i < getQtyOrdered(); i++) System.out.println((i + 1) + ". DVD - " + this.itemsOrdered[i].getTitle() + " - " + this.itemsOrdered[i].getCategory() + " - " + this.itemsOrdered[i].getDirector() + " - " + this.itemsOrdered[i].getLength() + ": " + this.itemsOrdered[i].getCost());
		System.out.println("Total cost: " + totalCost());
		System.out.println("***************************************************");
	}
}
