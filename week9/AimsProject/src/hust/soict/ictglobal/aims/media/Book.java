package hust.soict.ictglobal.aims.media;

import java.util.*;

public class Book extends Media implements Comparable{
	
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();
	//
	public Book() {
		// TODO Auto-generated constructor stub
	}
	//
	public Book(String title){
		super();
		this.title = title;
	}
	public Book(String title, String category){
		super();
		this.title = title;
		this.category = category;
	}
	public Book(String title, String category, List<String> authors){
		super();
		this.title = title;
		this.category = category;
		this.authors = authors;
	}
	//
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName) {
		if (this.authors.contains(authorName)) System.out.println("This author is already exist");
		else authors.add(authorName);
	}
	public void removeAuthor(String authorName) {
		if (!authors.remove(authorName)) System.out.println("This list doesn't have any author like that");
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
		processContent();
	}
	//
	public void processContent() {
		contentTokens = Arrays.asList(content.split("[\\p{Punct}\\s]+"));
		Collections.sort(contentTokens);

		System.out.println(contentTokens);

		for (String contentToken : contentTokens) {
			if (wordFrequency.containsKey(contentToken)) {
				wordFrequency.put(contentToken, wordFrequency.get(contentToken) + 1);
	    	} else {
	    		wordFrequency.put(contentToken, 1);
	    	}
	    }

	    for (Map.Entry entry : wordFrequency.entrySet()) {
	    	System.out.println(entry.getKey() + " " + entry.getValue());
	    }
	}
	//
	public int compareTo(Object obj) {
		Media media = (Media) obj;

	    return this.getTitle().compareTo(media.getTitle());
	}
}
