package week3;

public class Order {
	final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered;

	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		int n = getQtyOrdered();
		if (n < MAX_NUMBERS_ORDERED) {
			itemsOrdered[n] = disc;
			setQtyOrdered(n+1);
			System.out.println("The disc " + disc.getTitle() + " has been added");
		} else {
			System.out.println("The disc is already full");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int i = 0,
			n = getQtyOrdered();
		while (i < n) {
			if (itemsOrdered[i] == disc) {
				for (int j = i; j < n-1; j++) itemsOrdered[j] = itemsOrdered[j+1];
				setQtyOrdered(n-1);
				System.out.println("The disc " + disc.getTitle() + " has been removed");
				return;
			}
			i++;
		}
		System.out.println("There are no " + disc.getTitle() + " in this order");
	}
	public float totalCost() {
		float res = 0;
		for (int i = 0; i < getQtyOrdered(); i++) res += itemsOrdered[i].getCost();
		return res;
	}
}
