import java.util.*;

class Matrice{
	int width;
	int height;
	int matrice[][];
	
	Matrice(int width, int height){
		this.height = height;
		this.width = width;
		this.matrice = new int[height][width];
	}
	
	Matrice() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Height of matrice: ");
		this.height = scanner.nextInt();
		System.out.print("Width of matrice: ");
		this.width = scanner.nextInt();
		this.matrice = new int[height][width];
	}
	
	void inputMatriceFromKeyboard() {
		Scanner scan = new Scanner(System.in);
		for(int i = 0; i < height; i++)
			for(int j = 0; j < width; j++) {
				System.out.print("matrice[" + i + "][" + j + "] = ");
				matrice[i][j] = scan.nextInt();
			}
	}
	
	void printMatrice() {
		for(int i = 0;i < height; i++) {
			for(int j = 0; j <w idth; j++)
				System.out.print(matrice[i][j] + " ");
			System.out.println("");
		}
	}
}

class MatriceOperation{
	Matrice matrice1;
	Matrice matrice2;
	
	
	MatriceOperation(Matrice matrice1, Matrice matrice2){
		this.matrice1 = matrice1;
		this.matrice2 = matrice2;
	}
	
	Matrice addMatrice(){
		if((matrice1.height != matrice2.height) || (matrice1.width != matrice2.width)) {
			System.out.println("Invalid");
		}
		Matrice sum = new Matrice(matrice1.height, matrice1.width);
		
		for(int i = 0; i < matrice1.height; i++)
			for(int j = 0; j < matrice1.width; j++) {
				sum.matrice[i][j] = matrice1.matrice[i][j] + matrice2.matrice[i][j];
			}
		return sum;
	}
}

public class Exercise_7 {
	public static void main(String[] args) {
		Matrice matrice1 = new Matrice();
		matrice1.inputMatriceFromKeyboard();
		matrice1.printMatrice();
		
		System.out.println("");
		
		Matrice matrice2 = new Matrice();
		matrice2.inputMatriceFromKeyboard();
		matrice2.printMatrice();
		
		Matrice sum = new MatriceOperation(matrice1, matrice2).addMatrice();
		System.out.println("");
		sum.printMatrice();
	}
}
