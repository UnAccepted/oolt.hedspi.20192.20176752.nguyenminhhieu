package hust.soict.ictglobal.date;
import java.util.Date;

public class DateUtils {
	public static int compareDate(Date a, Date b) {
		if (a.compareTo(b) > 0) return 1;
		else if (a.compareTo(b) < 0) return -1;
		else return 0;
	}
	//
	public static void sortDates (Date[] listDates) {
		quickSort(listDates, 0, listDates.length - 1);
	}
	//
	public static void quickSort(Date[] arr, int start, int end){
		 
        int partition = partition(arr, start, end);
 
        if(partition - 1 > start) {
            quickSort(arr, start, partition - 1);
        }
        if(partition + 1 < end) {
            quickSort(arr, partition + 1, end);
        }
    }
    public static int partition(Date[] arr, int start, int end){
        Date pivot = arr[end];
 
        for (int i = start; i < end; i++){
            if (compareDate(arr[i], pivot) < 0) {
            	Date temp= arr[start];
                arr[start]=arr[i];
                arr[i]=temp;
                start++;
            }
        }
        Date temp = arr[start];
        arr[start] = pivot;
        arr[end] = temp;
        return start;
    }
}
