//Second degree equation one variable
package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;
import java.lang.Math;
//
public class SDE1V {
  public static void main(String[] args) {
    JOptionPane.showMessageDialog(null,"a * x^2 + b * x + c = 0", "Caculate", JOptionPane.INFORMATION_MESSAGE);
    String stra = JOptionPane.showInputDialog(null, "Please input the a number: ", "Input the a number", JOptionPane.INFORMATION_MESSAGE),
    strb = JOptionPane.showInputDialog(null, "Please input the b number: ", "Input the b number", JOptionPane.INFORMATION_MESSAGE),
    strc = JOptionPane.showInputDialog(null, "Please input the c number: ", "Input the c number", JOptionPane.INFORMATION_MESSAGE);
    Double a = Double.parseDouble(stra),
    b = Double.parseDouble(strb),
    c = Double.parseDouble(strc);
    Double temp = b*b - 4*a*c;
    if (a == 0) JOptionPane.showMessageDialog(null,"This isn't a Second degree equation one variable", "Result", JOptionPane.INFORMATION_MESSAGE);
    else  if (temp < 0) JOptionPane.showMessageDialog(null,"No Solution", "Result", JOptionPane.INFORMATION_MESSAGE);
    else {
      if (temp == 0) JOptionPane.showMessageDialog(null,"x = " + (-b / (2 * a)), "Result", JOptionPane.INFORMATION_MESSAGE);
      else JOptionPane.showMessageDialog(null,"x1 = " + ((-b + Math.sqrt(temp)) / (2 * a)) + " and x2 = " + ((-b - Math.sqrt(temp)) / (2 * a)), "Result", JOptionPane.INFORMATION_MESSAGE);
    }
    System.exit(0);
  }
}
