package hust.soict.ictglobal.garbage;

import java.io.*;
import java.util.Scanner; 

public class NoGarbage {
	public static void main(String[] args) throws Exception 
		{ 
			// pass the path to the file as a parameter 
			File file = new File("garbage.txt"); 
			Scanner sc = new Scanner(file); 
			long start = System.currentTimeMillis();
			StringBuffer data = new StringBuffer();
			while (sc.hasNextLine()) {
				data.append(sc.nextLine());
		        //System.out.println(data);
			}
			System.out.println(System.currentTimeMillis() - start);
	  }
}
