//First degree equation one variable
import javax.swing.JOptionPane;
//
public class FDE1V {
  public static void main(String[] args) {
    JOptionPane.showMessageDialog(null,"a * x + b = 0", "Caculate", JOptionPane.INFORMATION_MESSAGE);
    String stra = JOptionPane.showInputDialog(null, "Please input the a number: ", "Input the a number", JOptionPane.INFORMATION_MESSAGE),
    strb = JOptionPane.showInputDialog(null, "Please input the b number: ", "Input the b number", JOptionPane.INFORMATION_MESSAGE);
    Double a = Double.parseDouble(stra),
    b = Double.parseDouble(strb);
    if (a == 0) {
      if (b == 0) JOptionPane.showMessageDialog(null,"Infiniti Solution", "Result", JOptionPane.INFORMATION_MESSAGE);
      else JOptionPane.showMessageDialog(null,"No Solution", "Result", JOptionPane.INFORMATION_MESSAGE);
    } else JOptionPane.showMessageDialog(null,"Solution x = " + ((-b)/a), "Result", JOptionPane.INFORMATION_MESSAGE);
    System.exit(0);
  }
}
