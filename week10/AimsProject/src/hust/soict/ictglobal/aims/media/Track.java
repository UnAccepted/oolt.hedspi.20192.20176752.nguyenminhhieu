package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public class Track implements Playable, Comparable{
	
	private String title;
	private int length;
	//
	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}
	//
	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERROR: DVD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	//
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	//
	public int compareTo(Object obj) {
		Track track = (Track) obj;

	    return this.getTitle().compareTo(track.getTitle());
	}
}
