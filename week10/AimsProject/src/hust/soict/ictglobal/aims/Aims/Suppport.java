package hust.soict.ictglobal.aims.Aims;

import hust.soict.ictglobal.aims.media.*;
import hust.soict.ictglobal.aims.order.Order.*;
import hust.soict.ictglobal.aims.*;
import java.util.*;

public class Suppport {

	private Scanner scan = new Scanner(System.in);
	private List<Order> ordersList = new ArrayList<Order>();
	private List<DigitalVideoDisc> digitalVideoDiscList = new ArrayList<DigitalVideoDisc>();
	private List<Book> bookList = new ArrayList<Book>();
	private List<CompactDisc> compactDiscList = new ArrayList<CompactDisc>();
	private List<Track> trackList = new ArrayList<Track>();
	//
	//
	public void setListItems() {
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setId(0);
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		digitalVideoDiscList.add(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setId(1);
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		digitalVideoDiscList.add(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setId(2);
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("Join Musker");
		dvd3.setLength(90);
		digitalVideoDiscList.add(dvd3);
		
		Book book1 = new Book("Tu tot den vi dai", "Xa Hoi");
		book1.setId(0);
		book1.setCost(10);
		ArrayList<String> author1 = new ArrayList<String>();
		author1.add("Jim Collins");
		book1.setAuthors(author1);
		bookList.add(book1);
		
		CompactDisc cd1 = new CompactDisc("Top 10 Nhac Dan Truong");
		cd1.setId(0);
		cd1.setCategory("Music");
		cd1.setCost(100f);
		cd1.setDirector("MP3VN");
		cd1.setLength(60);
		cd1.setArtist("Dan Truong");
		compactDiscList.add(cd1);
		
		Track track1 = new Track("Di ve noi xa", 3);
		trackList.add(track1);
		//
		java.util.Collection collection = new java.util.ArrayList();
		collection.add(dvd1);
		collection.add(dvd2);
		collection.add(dvd3);
		
		java.util.Iterator iterator = collection.iterator();
		System.out.println("--------------------------------");
		while (iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		System.out.println("--------------------------------");
		java.util.Collections.sort((java.util.List)collection);
		iterator = collection.iterator();
		while (iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		System.out.println("--------------------------------");
	}
	//
	private void showMenu() {
		System.out.println("--------------------------------");
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.print("Please choose a number: ");
	}
	public void startchoice() {
		int choice = 0;
		setListItems();
		do {
			showMenu();
			choice = scan.nextInt();
			switch (choice) {
			case 1:
				System.out.println("--------------------------------");
				System.out.println("1. Create new order");
				int tmp1 = new Order(0).nbOrders,
					tmp2 = new Order(0).MAX_LIMITTED_ORDERS;
				if (tmp1 < tmp2) {
					createOrder();
				} else System.out.println("Cant create a new order");
				break;
			case 2:
				System.out.println("--------------------------------");
		        System.out.println("2. Add item to the order");
		        addItem();
		        break;
			case 3:
				System.out.println("--------------------------------");
		        System.out.println("3. Delete item by id");
		        deleteItem();
		        break;
			case 4:
				System.out.println("--------------------------------");
		        System.out.println("4. Display the items list of order");
		        printList();
		        break;
			case 0:
				System.out.println("--------------------------------");
		        System.out.println("Exit!");
		        System.out.println("--------------------------------");
		        break;
			default:
				System.out.println("--------------------------------");
				System.out.println("Invalid!");
		        break;
			}
		} while (choice != 0);
	}
	//
	private void createOrder() {
		Order order = new Order(1);
        ordersList.add(order);
	}
	//
	private void addItem() {
		int n = new Order(0).nbOrders;
		int choice = 0, choice1, choice2, tmp;
		if (n == 0) System.out.println("There is no order in list");
		else {
			System.out.println("OrderList's ID:");
			for (int i = 0; i < n; i++) System.out.println(ordersList.get(i).getId());
			System.out.print("Your ID choice: ");
			choice = scan.nextInt();
			if (choice < 0 || choice >= n) System.out.println("There are no Order's ID like that");
			else {
				tmp = choice;
				do {
					showAddMenu();
					choice = scan.nextInt();
					switch (choice) {
					case 1:
						System.out.println("--------------------------------");
						System.out.println("1. Book");
						System.out.println("Book's Items: ");
						for (int i = 0; i < bookList.size(); i++) {
							System.out.print((i+1));
							bookList.get(i).printMedia();
						}
						System.out.print("Please choose a number: ");
						choice1 = scan.nextInt();
						choice1--;
						if (choice1 < bookList.size() && choice1 >= 0) ordersList.get(tmp).addMedia(bookList.get(choice1));
						break;
					case 2:
						System.out.println("--------------------------------");
						System.out.println("2. CompactDisc");
						System.out.println("CD's Items: ");
						for (int i = 0; i < compactDiscList.size(); i++) {
							System.out.print((i+1));
							compactDiscList.get(i).printMedia();
						}
						System.out.print("Please choose a number: ");
						choice1 = scan.nextInt();
						scan.nextLine();
						choice1--;
						if (choice1 < compactDiscList.size() && choice1 >= 0) 
						{
							String c = new String();
							do {
								System.out.println("Do you want to add Track to CD? (y or n)");
								c = scan.nextLine();
								if (c.equalsIgnoreCase("y")) {
									System.out.println("Track's Items: ");
									for (int i = 0; i < trackList.size(); i++) {
										System.out.println((i+1) + ".Track - " + trackList.get(i).getTitle() + " - " + trackList.get(i).getLength());
									}
									System.out.print("Please choose a number: ");
									choice2 = scan.nextInt();
									scan.nextLine();
									choice2--;
									if (choice2 < trackList.size() && choice2 >= 0) compactDiscList.get(choice1).addTrack(trackList.get(choice2));
									System.out.println("Do you want to play Track? (y or n)");
									c = scan.nextLine();
									if (c.equalsIgnoreCase("y")) try {
										compactDiscList.get(choice1).play();
									} catch (PlayerException e){
										e.printStackTrace();
										e.getMessage();
										e.toString();
									}
								}
							} while (c.equalsIgnoreCase("n"));
							ordersList.get(tmp).addMedia(compactDiscList.get(choice1));
						} else System.out.println("There are no CD like that");
						break;
					case 3:
						System.out.println("--------------------------------");
						System.out.println("3. DigitalVideoDisc");
						System.out.println("DVD's Items: ");
						for (int i = 0; i < digitalVideoDiscList.size(); i++) {
							System.out.print((i+1));
							digitalVideoDiscList.get(i).printMedia();
						}
						System.out.print("Please choose a number: ");
						choice1 = scan.nextInt();
						choice1--;
						if (choice1 < digitalVideoDiscList.size() && choice1 >= 0) ordersList.get(tmp).addMedia(digitalVideoDiscList.get(choice1));
						break;
					case 0:
						System.out.println("--------------------------------");
						System.out.println("0. Quit");
						System.out.println("--------------------------------");
						break;
					default:
						System.out.println("--------------------------------");
						System.out.println("Invalid!");
						break;
					}
				} while (choice != 0);
			}
		}
	}
	public void showAddMenu() {
		System.out.println("--------------------------------");
		System.out.println("Item's type: ");
		System.out.println("--------------------------------");
		System.out.println("1. Book");
		System.out.println("2. CompactDisc");
		System.out.println("3. DigitalVideoDisc");
		System.out.println("0. Quit");
		System.out.println("--------------------------------");
		System.out.print("Please choose a number: ");
	}
	//
	private void deleteItem() {
		int choice = 0;
		int n = new Order(0).nbOrders;
		if (n == 0) System.out.println("There is no order in list");
		else {
			System.out.println("OrderList's ID:");
			for (int i = 0; i < n; i++) System.out.println(ordersList.get(i).getId());
			System.out.print("Your ID choice:");
			choice = scan.nextInt();
			if (choice < 0 || choice >= n) System.out.println("There are no Order's ID like that");
			else {
				n = ordersList.get(choice).itemsOrdered.size();
				if (n == 0) System.out.println("There is no item in list");
				else {
					System.out.println("ItemsList's ID:");
					for (int i = 0; i < n; i++) System.out.println(ordersList.get(choice).itemsOrdered.get(i).getId());
					System.out.print("Your ID choice:");
					choice = scan.nextInt();
					
					if (choice >= n && choice < 0) System.out.println("There are no media's id like that");
					else {
						ordersList.get(choice).itemsOrdered.remove(choice);
						System.out.println("The Media have been remove");
					}
				}
			}
		}
	}
	//
	private void printList() {
		int choice = 0;
		int n = new Order(0).nbOrders;
		if (n == 0) System.out.println("There is no order in list");
		else {
			System.out.println("OrderList's ID:");
			for (int i = 0; i < n; i++) System.out.println(ordersList.get(i).getId());
			System.out.print("Your ID choice:");
			choice = scan.nextInt();
			if (choice < 0 || choice >= n) System.out.println("There are no Order's ID like that");
			else {
				Order order = ordersList.get(choice);
				order.printOrder();
			}
		}
	}
	//
}
