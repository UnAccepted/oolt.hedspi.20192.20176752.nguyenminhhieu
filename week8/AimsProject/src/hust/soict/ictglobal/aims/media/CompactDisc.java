package hust.soict.ictglobal.aims.media;

import java.util.*;

public class CompactDisc extends Disc implements Playable, Comparable{

	public CompactDisc(String title) {
		super();
		this.title = title;
	}
	public CompactDisc() {
		
	}
	private String artist;
	private int length;
	private List<Track> tracks = new ArrayList<Track>();
	//
	public void addTrack(Track track) {
		for (int i = 0; i < tracks.size(); i++) if (tracks.get(i) == track) {
			System.out.println("This track has already existed");
			return;
		}
		tracks.add(track);
	}
	public void removeTrack(Track track) {
		for (int i = 0; i < tracks.size(); i++) if (tracks.get(i) == track) {
			tracks.remove(i);
			System.out.println("The track have been removed");
			return;
		}
		System.out.println("The track doesn't exist");
	}
	//
	public void play() {
		System.out.println(this.artist + "'s CD play:");
		for (int i = 0; i < tracks.size(); i++) tracks.get(i).play();
	}
	//
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getLength() {
		int res = 0;
		for (int i = 0; i < tracks.size(); i++) res += tracks.get(i).getLength();
		return res;
	}
	//
	public int compareTo(Object obj) {
		CompactDisc cd = (CompactDisc) obj;

	    if (tracks.size() > cd.tracks.size()) return 1;
	    else if (tracks.size() < cd.tracks.size()) return -1;
	    else {
	      if (this.getLength() > cd.getLength()) return 1;
	      else if (this.getLength() < cd.getLength()) return -1;
	      return 0;
	    }
	}
}
