package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable{
	public DigitalVideoDisc(String title, String category, String director) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
	}
	public DigitalVideoDisc(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}
	public DigitalVideoDisc(String title) {
		super();
		this.title = title;
	}
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	public DigitalVideoDisc(String title, String category, String director, int length) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
	}
	//
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	//
	public boolean search(String title) {
		title = title.trim();
		String tmp = this.title;
		tmp = tmp.trim();
		String[] tmp1 = title.split("\\s"),
				tmp2 = tmp.split("\\s");
		for (int i = 0; i < tmp1.length; i++)
			for (int j = 0; j < tmp2.length; j++)
				if (tmp1[i].equalsIgnoreCase(tmp2[j])) return true;
		return false;
	}
	//
	public int compareTo(Object obj) {
		DigitalVideoDisc dvd = (DigitalVideoDisc) obj;

	    if (this.getCost() > dvd.getCost()) return 1;
	    else if (this.getCost() < dvd.getCost()) return -1;
	    return 0;
	}
}
