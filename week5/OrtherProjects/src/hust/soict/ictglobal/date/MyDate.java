package hust.soict.ictglobal.date;
import java.util.*;
import java.text.*;

public class MyDate {
	private int day;
	private int month;
	private int year;
	private Date date;
	
	String[] monthInString = new String[]{"x", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"},
			dayInString = new String[] {"x", "1st", "2nd", "3th", "4th", "5th", "6th", "7th", "8th", "9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th", "21th", "22th", "23th", "24th", "25th", "26th", "27th", "28th", "29th", "30th", "31th"},
			dayInString2 = new String[] {"x", "first", "second", "third","fourth","fifth","sixth","seventh","eighth","ninth","tenth", "eleventh","twelfth","thirteenth","fourteenth","fifteenth","sixteenth","seventeenth","eighteenth","nineteenth", "twentieth","twenty first","twenty second","twenty third","twenty fourth","twenty fifth","twenty sixth","twenty seventh", "twenty eighth","twenty ninth","thirtieth","thirty first"};
	public MyDate() {
		Calendar calendar = Calendar.getInstance();
		this.day = calendar.get(Calendar.DATE);
		this.month = calendar.get(Calendar.MONTH)+1;
		this.year = calendar.get(Calendar.YEAR);
	}
	
	public MyDate(int day, int month, int year) {
		if (new CheckYMD().checkYMD(year, month, day)) {
			this.day = day;
			this.month = month;
			this.year = year;
		} else System.out.println("Invalid Date!");
	}	
	//Work to do: have to check valid date
	public MyDate(String date) {
		dateInStringToDate(date);
	}
	
	public void accept() {
		String date;
		Scanner scan = new Scanner(System.in);
		System.out.print("Input date (EX: February 18th 2019): ");
		date = scan.nextLine();
		dateInStringToDate(date);
		scan.close();
	}
	//
	private void dateInStringToDate(String date) {
		int i, d, m, y;
		//February 18th 2019
		date = date.trim();
		String[] dateInString = date.split("\\s");//splits the string based on whitespace 
		
		//check valid of month
		i = 1;
		String monthInLowerCase = dateInString[0].toLowerCase();
		while((i <= 12) && (monthInLowerCase.equals(monthInString[i]) == false)) i=i+1;

		if(i > 12) {
			System.out.println("Invalid date!");
			return;
		} else m = i;
		
		//check valid of day
		i = 1;
		String dayInLowerCase = dateInString[1].toLowerCase();
		while((i <= 31) && (dayInLowerCase.equals(dayInString[i]) == false)) i=i+1;
		
		if(i > 31) {
			System.out.println("Invalid date!");
			return;
		} else d = i;
		
		y = Integer.parseInt(dateInString[2]);
		
		if (new CheckYMD().checkYMD(y, m, d)) {
			this.day = d;
			this.month = m;
			this.year = y;
		} else System.out.println("Invalid Date!");
	}
	//
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public void setDay(String sDay) {
		String sDayInString = sDay.toLowerCase();
		int i = 0;
		while((i <= 31)&&(sDayInString.equals(dayInString[i]) == false)) i++;
		if(i > 31) {
			System.out.println("Invalid day!");
			return;
		} else this.day = i;
	}
	//
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public void setMonth(String sMonth) {
		String sMmonthInString=sMonth.toLowerCase();
		int i = 0;
		while((i <= 12)&&(sMmonthInString.equals(monthInString[i]) == false)) i++;
		if(i > 12) {
			System.out.println("Invalid month!");
			return;
		}else this.month = i;
	}
	//
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void setYear(String sYear) {
		int year = new ConvertStringToYear().convertStringToYear(sYear);
		if(year <= 0)
			System.out.println("Invalid year!");
		else
			this.year = year;
	}
	//
	public Date getDate( ) {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	//
	public Date convertDate() {
		SimpleDateFormat temp = new SimpleDateFormat("dd/MM/yyyy");
		Date res = new Date();
		String currentDate = new String();
		currentDate = String.valueOf(getDay()) + "/" + String.valueOf(getMonth()) + "/" + String.valueOf(getYear());
		try {
			 res = temp.parse(currentDate);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public void print() {
		if (new CheckYMD().checkYMD(getYear(), getMonth(), getDay())) System.out.println("Current date is: " + this.day + "-" + this.month + "-" + this.year);
		else System.out.println("Invalid Date to print!");
	}
	//
	public void printForm() {
		System.out.println("Form 1: Date Format using getInstance(): 31/3/15 2:37 PM");
		System.out.println("Form 2: Date Format using getDateInstance(): 31 Mar, 2015");
		System.out.println("Form 3: Date Format using getTimeInstance(): 2:37:23 PM");
		System.out.println("Form 4: Date Format using getDateTimeInstance(): 31 Mar, 2015 2:37:23 PM");
		System.out.println("Form 5: Date Format using getTimeInstance(DateFormat.SHORT): 2:37 PM");
		System.out.println("Form 6: Date Format using getTimeInstance(DateFormat.MEDIUM): 2:37:23 PM");
		System.out.println("Form 7: Date Format using getTimeInstance(DateFormat.LONG): 2:37:23 PM IST");
		System.out.println("Form 8: Date Format using getDateTimeInstance(DateFormat.LONG,DateFormat.SHORT): 31 March, 2015 2:37 PM");
	}
	public void print(int choice) throws java.text.ParseException{
		String sDate = "";
		if(day < 10) sDate = sDate + "0" + day;
		else sDate = sDate+day;
		if(month < 10) sDate = sDate + "/0" + month;
		else sDate=sDate + "/" + month;
		sDate = sDate + "/" + year;
		
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
		
		 String dateToStr;
		 
		 switch(choice) {
		 case 1:
			 dateToStr = DateFormat.getInstance().format(date);  
			 System.out.println("Current date is: "+dateToStr); 
			 break;
		 case 2:
			 dateToStr = DateFormat.getDateInstance().format(date);  
		     System.out.println("Current date is: "+dateToStr);
			 break;
		 case 3:
			 dateToStr = DateFormat.getTimeInstance().format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 case 4:
			 dateToStr = DateFormat.getDateTimeInstance().format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 case 5:
			 dateToStr = DateFormat.getTimeInstance(DateFormat.SHORT).format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 case 6:
			 dateToStr = DateFormat.getTimeInstance(DateFormat.MEDIUM).format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 case 7:
			 dateToStr = DateFormat.getTimeInstance(DateFormat.LONG).format(date);  
		        System.out.println("Current date is: "+dateToStr); 
			 break;
		 case 8:
			 dateToStr = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.SHORT).format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 default:
				break;
		 }
	}

}
