import java.util.*;

public class Exercise_4 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("In put the height: ");
		int height;
		height = scan.nextInt();

		int i,j;
		for(j = height, i = height; j >= 1; j--, i++) {
			int countCol;
			for(countCol = 0; countCol <= i; countCol++)
				if((countCol >= j) && (countCol <= i)) System.out.print("*");
				else System.out.print(" ");
			System.out.println("");
		}
		scan.close();
	}
}
