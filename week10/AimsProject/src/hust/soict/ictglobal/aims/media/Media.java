package hust.soict.ictglobal.aims.media;

import java.lang.*;

public abstract class Media implements Comparable {

	public Media(String title) {
		super();
		this.title = title;
	}

	public Media(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}

	public Media(String title, String category, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	//
	protected int id;
	protected String title, category;
	protected float cost;
	
	public Media() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	//
	public void printMedia() {
		System.out.println(". Media - " + getTitle() + " - " + getCategory() + " - " + getCost());
	}
	//
	@Override
	public boolean equals (Object obj) {
		if (obj != null && obj instanceof Media) {
			Media media = (Media) obj;
			if (this.title == media.getTitle() && this.cost == media.getCost()) return true;
		}
		return false;
	}
	//
	@Override
	public int compareTo(Object obj) {
		if (obj != null && obj instanceof Media) {
			Media media = (Media) obj;
			if (this.title.compareTo(media.getTitle()) > 0) return 1;
			else if (this.title.compareTo(media.getTitle()) < 0) return -1;
			else if (this.cost > media.getCost()) return 1;
			else if (this.cost < media.getCost()) return -1;
			else return 0;
		}
		return -2;
	}
}
