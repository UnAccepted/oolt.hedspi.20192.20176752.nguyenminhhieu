package hust.soict.ictglobal.aims.media;

public abstract class Media {

	public Media(String title) {
		super();
		this.title = title;
	}

	public Media(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}

	public Media(String title, String category, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	//
	protected int id;
	protected String title, category;
	protected float cost;
	
	public Media() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	//
	public void printMedia() {
		System.out.println(". Media - " + getTitle() + " - " + getCategory() + " - " + getCost());
	}
}
