package week3;

public class CheckYMD {
	public boolean checkYMD(int year, int month, int day) {
		if((year <= 0) || ((month <= 0) || (month > 12)) || ((day <= 0) || (day > (new DayOfMonth(month,year).getMaxDay())))) {
			return false;
		}
		return true;
	}
}
