package hust.soict.ictglobal.lab02;
import java.util.*;
import java.math.*;

class DayOfMonth{
	boolean result = true;
	
	DayOfMonth(int month, int year){
		if(year <= 0) 
			result = false;
		else switch(month) {
		case 1:case 3:case 5:case 7:case 8:case 10:case 12:
			System.out.println("31 days");
			break;
		case 4:case 6:case 9:case 11:
			System.out.println("30 days");
			break;
		case 2:
			if((year%4 == 0) && (year%100 != 0))
				System.out.println("29 days");
			else System.out.println("28 days");
			break;
		default:
			result=false;
		}
	}
}

public class Exercise_5 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		boolean result;
		do {
			System.out.print("Year: ");
			int year = scan.nextInt();
			System.out.print("Month: ");
			int month = scan.nextInt();
			result = new DayOfMonth(month, year).result;
			if(result == false) {
				System.out.println("");
				System.out.println("Invalid. Please reinput!");
				System.out.println("");
			}
		}while(result == false);
		scan.close();
	}
}
